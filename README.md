# Vision-based Drone Landing System


##About

### System Components

1. Control App - APASLab Aviator
2. Base App - APASLab Base
3. Helper App - APASLab Base Flight Info

### Briefly

The primary smartphone with APASLab Aviator app installed communicates with the quadcopter via signal extender. The phone receives information from the aircraft and sends commands to it. By sending landing command APASLab Aviator app begins communicating with the secondary smartphone running APASLab Base app.

The secondary phone is located on the landing site with camera facing straight up. It receives requests from the control app and returns the current location of the quadcopter in the local coordinates that are calculated using methods of computer vision. The goal of the control app is then to guide the quadcopter to the origin of the local coordinate system which represents the center of the image.

When control app requests quadcopter's location from the base app, the base app also sends current coordinates to the helper app. That data is then stored in a database and can be presented graphically.

For more detailed explanation refer to *Krmpotić M.: "Vizualno potpomognuti sustav za slijetanje bespilotne letjelice", Final Work, Faculty of Engineering, University of Rijeka, 2015* or [send an email directly to Marin](mailto:marin.krmpotic@gmail.com).


## Prequisites

### Equipment required

1. DJI Phantom 2 Vision+ quadcopter
2. Android >=4.0 device with APASLab Aviator app installed
3. Android >=2.2 device with APASLab Base app installed
4. [OPTIONAL] PHP/MySQL-enabled server

### IDEs Recommended

1. Android Studio for the APASLab Aviator app
2. Eclipse for the APASLab Base app
3. Any text editor for the helper app (if applicable)


## Installation Instructions

Each of the three main application folders contains a README file with installation instructions.

## Usage Instructions

1. In APASLab Aviator app select the flight route and tap on *Set Route*.
2. Open the APASLab Base app on the second device and by taping on the phone's menu button open the connectivity menu and pair with the primary device.
3. Place the second device on the ground facing up oriented exactly to the North.
4. In the APASLab Aviator App tap on *Start*.
5. Tap on the *Go Home* button to start the landing sequence.


## Contributors

1. Marin Krmpotić (APASLab Aviator 1.0, APASLab Base 1.0, initial version of APASLab Base Flight Info)





