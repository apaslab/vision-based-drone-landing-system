# APASLab Base


##Installation Instructions

When the first lines of code of the APASLab Base were written Eclipse was still the most used IDE for Android development. That has now changed so **it is recommended to migrate this project to Android Studio**, although not strictly necessary.

To import a project to Eclipse with Android Developer Tools (ADT) installed:

1. Go to *File > Import...*
2. Expand *Android* and select *Existing Android Code Into Workspace*, click *Next >*
3. Click *Browse* and select the *Base App* folder.
4. Under *Projects* there should now be selected two items: *APASLabBase* and *OpenCD Library-2.4.10*, click *Finish*

Done! :)

