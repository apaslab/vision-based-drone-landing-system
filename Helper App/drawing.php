<?php
// SPAJANJE NA BAZU
try {
        //Spoji se na bazu hsahr_aviator sa korisničkim imenom <user> i lozinkom <lozinka>
        $db = new PDO("mysql:dbname=hsahr_aviator;", "<user>", "<lozinka>");
        //Želimo da pri pojavi greške, PDO baci iznimku
        $db->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
        );
} catch (PDOException $e) {
        //Ako se nismo spojili na bazu
        die("Nismo se spojili na bazu!");
}


try {
        //Postavi encoding veze na utf8
        $db->exec("SET NAMES utf8");
        
        } catch(PDOException $e) {
        //Ako se dogodila neka greška
        die("Error: {$e->getMessage()}");
}

  $query = $db->prepare("SELECT * FROM points WHERE level=? ORDER BY id ASC");
  $query->setFetchMode(PDO::FETCH_OBJ);
  $query->execute(array($_GET['l']));

  header('Content-type: image/png');

  $png_image = imagecreate(900, 600);

  imagecolorallocate($png_image, 255, 255, 255);
  imagesetthickness($png_image, 1);
  $black = imagecolorallocate($png_image, 0, 0, 0);

  $x = 0;
  $y = 0;
  $w = imagesx($png_image) - 1;
  $z = imagesy($png_image) - 1;


  imageline($png_image, $x, imagesy($png_image)/2, $w, imagesy($png_image)/2, $black);
  imageline($png_image, imagesx($png_image)/2, $y, imagesx($png_image)/2, $z, $black);


  // choose a color for the ellipse
  $col_ellipse = imagecolorallocate($png_image, 224, 25, 25);
  $col_white = imagecolorallocate($png_image, 255, 255, 255);
  
  $i = 1;
  $previousPointX = false;
  $previousPointY = false;
  foreach($query as $point) {
    if ($previousPointX && $previousPointY) {
      imageline($png_image, $previousPointX, $previousPointY, $point->x + 450, 300 - $point->y, $col_ellipse);
    }
    if ($point->type == 1) {
	$col_ellipse = imagecolorallocate($png_image, 224, 25, 25);
    } else if ($point->type == 2) {
	$col_ellipse = imagecolorallocate($png_image, 0, 0, 0);
    } else {
	$col_ellipse = imagecolorallocate($png_image, 0, 255, 0);
    }
    // draw the ellipse
    imagefilledellipse($png_image, $point->x + 450, 300 - $point->y, 15, 15, $col_ellipse);
    imagestring($png_image, 2, $point->x + 448, 294 - $point->y, $i, $col_white);
    imagestring($png_image, 2, $point->x + 428, 308 - $point->y, '(' . round($point->x, 2) . ', ' . round($point->y, 2) . ')', $black);
    //imagestring($png_image, 2, $point->x + 428, 318 - $point->y, date('H:i:s',strtotime($point->timestamp)), $black);
    $previousPointX = $point->x + 450;
    $previousPointY = 300 - $point->y;
    $i++;
  }


  imagepng($png_image);
  imagedestroy($png_image);
?> 
