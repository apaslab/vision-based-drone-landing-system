# APASLab Base Flight Info


##Installation Instructions

To use the helper app you need a running PHP server with MySQL database.

+ Import *hsahr_aviator.sql* in your database
+ Insert your database credentials in all of the three *.php* files.
+ Edit the *url* String variable under *MainActivity.java* file of the base app to the URL of your helper app.

To use the helper app your secondary smartphone has to be connected to the Internet. Between each flight you should empty the database table manually (or improve the app so you don't have to :)).


