<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
header('Content-Type: application/json');

// SPAJANJE NA BAZU
try {
        //Spoji se na bazu hsahr_aviator sa korisničkim imenom <user> i lozinkom <lozinka>
        $db = new PDO("mysql:dbname=hsahr_aviator;", "<user>", "<lozinka>");
        //Želimo da pri pojavi greške, PDO baci iznimku
        $db->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
        );
} catch (PDOException $e) {
        //Ako se nismo spojili na bazu
        die("Nismo se spojili na bazu!");
}


try {
        //Postavi encoding veze na utf8
        $db->exec("SET NAMES utf8");
        
        } catch(PDOException $e) {
        //Ako se dogodila neka greška
        die("Error: {$e->getMessage()}");
}

if (isset($_GET['x']) && !empty($_GET['x']) && isset($_GET['y']) && !empty($_GET['y']) && isset($_GET['l']) && !empty($_GET['l']) && isset($_GET['t']) && !empty($_GET['t'])) {


        $query = $db->prepare("INSERT INTO points (x, y, level, type) VALUES (?, ?, ?, ?)");
        $query->execute(array(((-1) * $_GET['x']), ((-1) * $_GET['y']), $_GET['l'], $_GET['t']));
	echo json_encode(array('status' => 1));
}

?>
