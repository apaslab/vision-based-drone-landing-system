<?php

// SPAJANJE NA BAZU
try {
        //Spoji se na bazu hsahr_aviator sa korisničkim imenom <user> i lozinkom <lozinka>
        $db = new PDO("mysql:dbname=hsahr_aviator;", "<user>", "<lozinka>");
        //Želimo da pri pojavi greške, PDO baci iznimku
        $db->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
        );
} catch (PDOException $e) {
        //Ako se nismo spojili na bazu
        die("Nismo se spojili na bazu!");
}


try {
        //Postavi encoding veze na utf8
        $db->exec("SET NAMES utf8");
        
        } catch(PDOException $e) {
        //Ako se dogodila neka greška
        die("Error: {$e->getMessage()}");
}

$query = $db->prepare("SELECT DISTINCT(level) FROM points ORDER BY level ASC");
$query->setFetchMode(PDO::FETCH_OBJ);
$query->execute();

  if(isset($_GET['l']) && !empty($_GET['l'])) {
  	$level = $_GET['l'];
  } else {
  	$level = 1;
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
	<link href="css/main.css" rel="stylesheet"/>
	<title>APASLab Base Flight Info</title>
</head>
<body>
	<div id="page-container">
		<h1>APASLab Base Flight Info</h1>
		<h2>Level <?php echo $level ?></h2>
<img class="drawing" src="drawing.php?l=<?php echo $level ?>" alt="">

<nav>
<?php foreach($query as $level) { ?>
	<a href="?l=<?php echo $level->level ?>">Level <?php echo $level->level ?></a>	
<?php } ?> 
</nav>
</div>

</body>
</html>
