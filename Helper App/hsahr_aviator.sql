-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 15, 2015 at 09:58 AM
-- Server version: 5.5.42-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hsahr_aviator`
--

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE IF NOT EXISTS `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `level` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=33 ;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `x`, `y`, `level`, `timestamp`, `type`) VALUES
(1, 155, 38, 1, '2015-09-13 15:41:00', 2),
(2, 168, 48.5, 1, '2015-09-13 15:41:00', 1),
(3, 177, 45.5, 1, '2015-09-13 15:41:00', 2),
(4, 192, 23.5, 1, '2015-09-13 15:41:00', 2),
(5, 188, 12, 1, '2015-09-13 15:41:00', 2),
(6, 206, -0, 1, '2015-09-13 15:41:01', 2),
(7, 200, -16, 1, '2015-09-13 15:41:02', 3),
(8, 175.5, -10.5, 1, '2015-09-13 15:41:03', 3),
(9, 157.5, -10.5, 1, '2015-09-13 15:41:04', 3),
(10, 134, -16, 1, '2015-09-13 15:41:05', 3),
(11, 111.5, -14, 1, '2015-09-13 15:41:06', 3),
(12, 98.5, -11, 1, '2015-09-13 15:41:07', 3),
(13, 72.5, -18.5, 1, '2015-09-13 15:41:08', 3),
(14, 55, -27, 1, '2015-09-13 15:41:09', 3),
(15, 41.5, -21, 1, '2015-09-13 15:41:10', 3),
(16, 24, -22.5, 1, '2015-09-13 15:41:11', 3),
(17, 9, -19.5, 1, '2015-09-13 15:41:12', 3),
(18, 37.5, -54, 2, '2015-09-13 15:41:20', 1),
(19, 44.5, 100.5, 2, '2015-09-13 15:41:20', 2),
(20, 74.5, 81.5, 2, '2015-09-13 15:41:21', 3),
(21, -8.5, -24.5, 2, '2015-09-13 15:41:22', 3),
(22, 0.5, -76, 3, '2015-09-13 15:41:30', 1),
(23, 31, -80, 3, '2015-09-13 15:41:30', 2),
(24, 9.5, -6.5, 3, '2015-09-13 15:41:31', 2),
(25, 27.5, 41.5, 3, '2015-09-13 15:41:31', 3),
(26, -40, 83, 3, '2015-09-13 15:41:32', 3),
(27, -109.5, 18.5, 4, '2015-09-13 15:41:38', 1),
(28, -60, -0, 4, '2015-09-13 15:41:38', 2),
(29, -98, -11.5, 4, '2015-09-13 15:41:42', 3),
(30, 51.5, -28, 4, '2015-09-13 15:41:42', 3),
(31, -98, -11.5, 4, '2015-09-13 15:41:42', 3),
(32, -17, -0, 4, '2015-09-13 15:41:42', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
